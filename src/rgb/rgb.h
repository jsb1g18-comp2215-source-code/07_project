#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>

/**
 *   \file rgb.h
 *   \brief PWM Simulation and RGB LED Handling for AVR
 *
 *  Simulates PWM on the PORT and PINS Specified Below
 *
 */

/*****************************************************************************/
/*                     Ports and Pins for the Simulation                     */
/*****************************************************************************/
#define PORT PORTF // LED is connected to PORTF
#define DDR DDRF   // Use the PORTF Data Direction Register
#define RPIN PF0   // Pin 0 for Red
#define GPIN PF1   // Pin 1 for Green
#define BPIN PF2   // Pin 2 for Blue

/* Checks whether there is a bit match between two ints **********************/
#define _BITMATCH(a, b) (a & b)

/*****************************************************************************/
/*                             RGB Colour Struct                             */
/*****************************************************************************/
typedef struct {
  uint8_t R_VAL;
  uint8_t G_VAL;
  uint8_t B_VAL;
} RGB;



void os_init_rgb_pwm(void);
void resetRGB();
void setRGB(uint8_t, uint8_t, uint8_t);
int rgbPWMCycle(int); // Scheduled task

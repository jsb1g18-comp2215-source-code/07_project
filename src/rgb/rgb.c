#include "rgb.h"
#include <avr/io.h>

RGB currentColour;

void os_init_rgb_pwm(void) {
  // Configure ports
  DDR = _BV(RPIN) | _BV(GPIN) | _BV(BPIN);
  //PORT = _BV(RPIN) | _BV(GPIN) | _BV(BPIN);
}

void resetRGB(void) {
  currentColour.R_VAL = 0b00000001;
  currentColour.G_VAL = 0b00000001;
  currentColour.B_VAL = 0b00000001;
}

void setRGB(uint8_t r, uint8_t g, uint8_t b) {
  currentColour.R_VAL = r;
  currentColour.G_VAL = g;
  currentColour.B_VAL = b;
}

int rgbPWMCycle(int state) {
  uint8_t port_write = 0b00000000;
  if (_BITMATCH(currentColour.R_VAL, state) == state) {
    port_write |= _BV(RPIN);
  }
  if (_BITMATCH(currentColour.G_VAL, state) == state) {
    port_write |= _BV(GPIN);
  }
  if (_BITMATCH(currentColour.B_VAL, state) == state) {
    port_write |= _BV(BPIN);
  }
  state++;
  if (state > 255) {
    state = 0;
  }
  PORT = port_write;
  return state;
}

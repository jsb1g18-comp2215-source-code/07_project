/*  _____          _                      ___  ____
 * |  ___|__  _ __| |_ _   _ _ __   __ _ / _ \/ ___|
 * | |_ / _ \| '__| __| | | | '_ \ / _` | | | \___ \
 * |  _| (_) | |  | |_| |_| | | | | (_| | |_| |___) |
 * |_|  \___/|_|   \__|\__,_|_| |_|\__,_|\___/|____/
 *
 */


#include "os.h"
#include "rios.h"
#include "rgb.h"

void os_init(void) {
    /* 8MHz clock, no prescaling (DS, p. 48) */
    CLKPR = (1 << CLKPCE);
    CLKPR = 0;

    os_init_rgb_pwm();
    os_init_scheduler();
}

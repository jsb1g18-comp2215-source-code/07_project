/**
 *   \file main.c
 *   \brief Main file
 *
 *  Initialises the operating system
 *
 */


#include "os.h"
#include "rgb.h"

int check_switches(int);
int update_led_colour(int);

void main(void) {
  os_init();

  os_add_task(rgbPWMCycle, 1, 1);
  os_add_task(update_led_colour, 1000 * 10, 1);

  sei();
  for(;;) {}
}

int update_led_colour(int state) {
  if (state == 1) {
    setRGB(73, 60, 49);
    state = 2;
    return state;
  }
  if (state == 2) {
    setRGB(250, 36, 79);
    state = 3;
    return state;
  }
  if (state == 3) {
    setRGB(98, 195, 212);
    state = 1;
    return state;
  }
  return 1;
}
